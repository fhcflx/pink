#!/bin/sh

#Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::gitbook')"
Rscript -e "install.packages('devtools')"
Rscript -e "devtools::install_github('fhcflx/pinkstyle')"
Rscript -e "bookdown::render_book('index.Rmd')"
