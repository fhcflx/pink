---
title: "Pinkstyle para Bookdown (todo em pt-br)"
author: "Francisco Felix"
date: "`r Sys.Date()`"
knit: "bookdown::preview_chapter"
site: "bookdown::bookdown_site"
link-citations: yes
output:
  pinkstyle::pink_html_book:
    highlight: tango
    toc: TRUE
    toc_depth: 1
    split_by: chapter
    margin_references: FALSE
    includes:
      in_header: head.html
bibliography: [pink.bib]
biblio-style: apalike
description: "Um modelo html para bookdown adaptado do modelo MSMB HTML Book Style"
---

```{r, include=FALSE}
options(bookdown.post.latex =function(x) {
 gsub('(?<=\\\\includegraphics\\[)', 'fbox,', x, perl = TRUE)
})
```

\setmainfont{Comic Sans MS}

\includepdf[scale=1]{pdf page to insert.pdf}

# Preâmbulo


Este é um _exemplo_ escrito em **Markdown**. Pode-se usar qualquer sintaxe que o Markdown para Pandoc suporta, e.g., uma equação matemática $p^i + n^k = o^k$.

Pode-se instalar a versão de desenvolvimento de **bookdown** a partir do Github:

```{r eval=FALSE}
devtools::install_github("rstudio/bookdown")
```

Lembrar que cada arquivo Rmd contém um e apenas um capítulo, e este é definido como um cabeçalho de primeiro nível, denotado por `#`.

Para compilar este exemplo em PDF, é necessário instalar XeLaTeX.

```{r include=FALSE}
# automatically create a bib database for R packages
knitr::write_bib(c(
  .packages(), 'bookdown', 'knitr', 'rmarkdown'), 'packages.bib')
```

  [![pipeline status](https://img.shields.io/gitlab/pipeline/fhcflx/pink.svg?color=ef5ba2)](https://gitlab.com/fhcflx/pink/commits/master)
